package h_array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A {
    public static void main(String[] args) {

        int[][] arr = {{1,2,3},{11,22,33},{111,222,333},{1111,2222,3333}};
        for (int j = 0; j < arr.length; j++) {
            for (int k = 0; k < arr[j].length; k++) {
                System.out.print(arr[j][k]);
            }
            System.out.println();
        }

        int s[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        System.out.println("s: " + Arrays.toString(s));
        int d[] = { 15, 25, 35, 45, 55, 65, 75, 85, 95, 105};
        System.out.println("d: " + Arrays.toString( d));
        System.arraycopy(s, 1, d, 1, 8);
        System.out.println("-> " + Arrays.toString(d));

        int q[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        int w[] = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
        System.out.println(Arrays.equals(q, w));

        Arrays.fill(q, 33);
        System.out.println("Fill 'q' array - the same value: " + Arrays.toString(q));

        int t[] = { 35, 45, 15, 25, 75, 85,55, 65, 95, 105};
        Arrays.sort(t);
        System.out.println("sort(): " + Arrays.toString(t));

        int r[] = { 15, 25, 35, 45, 55, 65, 75, 85, 95, 105};
        int search = Arrays.binarySearch(r, 555);
        System.out.println("search index is " + search + " -> "
                + "search: " + (search < 0 ? "no" : ("yes - position: " + (search + 1))));

        int[] f = reverse(s);
        System.out.println("reverse() -> s: " + Arrays.toString(f));

    }

    public static int[] reverse(int[] list) {
        int[] result = new int[list.length];
        for (int i=0, j=result.length - 1; i<list.length; i++, j--) {
            result[j] = list[i];
        }
        return result;
    }

}
