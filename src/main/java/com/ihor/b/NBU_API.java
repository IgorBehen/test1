package com.ihor.b;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class NBU_API {
    public static void main(String[] args) throws IOException {
        URL nbu_api = new URL("https://old.bank.gov.ua/NBUStatService/v1/statdirectory/exchange");
        BufferedReader br = new BufferedReader(new InputStreamReader(nbu_api.openStream()));
        String tmp;
        while ((tmp = br.readLine()) != null){
            System.out.println(tmp);
        }
    }
}
