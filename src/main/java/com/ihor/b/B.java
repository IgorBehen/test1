package com.ihor.b;

import com.google.common.collect.Lists;

import java.util.*;
class Human{
    String name;
    int a;
    Human(String s, int a){
        this.name=s;
        this.a=a;
    }

    public String getS() {
        return name;
    }

    public int getA() {
        return a;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", a=" + a +
                '}';
    }
}

public class B implements Comparator<String> {
    public static void main(String[] args) {
        List<String> st = new ArrayList<String>();
        String a = "abc";
        String b = "ABC";
        st.add(a);
        st.add(b);

        List<Human> humans = Lists.newArrayList(
                new Human("Sarah", 10),
                new Human("Jack", 12)
        );

        System.out.println(humans);

        Collections.sort(humans, new Comparator<Human>() {
            @Override
            public int compare(Human h1, Human h2) {
                return h1.getName().compareTo(h2.getName());
            }
        });

        System.out.println(humans);
//Collections.sort(st, new Comparator<List<String>>()){
//
//
//    @Override
//    public int compare(String o1, String o2) {
//        /*Comparator<String> comp*/
//        int c = (a, b) -> a.compareTo(b);
//        return comp;
//    }
//}
//
//    @Override
//    public int compare(String o1, String o2) {
//        return 0;
    }

    @Override
    public int compare(String o1, String o2) {
        return 0;
    }
}
