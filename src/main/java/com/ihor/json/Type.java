package com.ihor.json;

import java.util.HashMap;
import java.util.Map;

public class Type {

    private String caramel;
    private CandyGroup candyGroup;
    private String iris;
    private String chocolate;
    private String isStuffed;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Type() {
    }

    /**
     *
     * @param isStuffed
     * @param candyGroup
     * @param chocolate
     * @param iris
     * @param caramel
     */
    public Type(String caramel, CandyGroup candyGroup, String iris, String chocolate, String isStuffed) {
        super();
        this.caramel = caramel;
        this.candyGroup = candyGroup;
        this.iris = iris;
        this.chocolate = chocolate;
        this.isStuffed = isStuffed;
    }

    public String getCaramel() {
        return caramel;
    }

    public void setCaramel(String caramel) {
        this.caramel = caramel;
    }

    public CandyGroup getCandyGroup() {
        return candyGroup;
    }

    public void setCandyGroup(CandyGroup candyGroup) {
        this.candyGroup = candyGroup;
    }

    public String getIris() {
        return iris;
    }

    public void setIris(String iris) {
        this.iris = iris;
    }

    public String getChocolate() {
        return chocolate;
    }

    public void setChocolate(String chocolate) {
        this.chocolate = chocolate;
    }

    public String getIsStuffed() {
        return isStuffed;
    }

    public void setIsStuffed(String isStuffed) {
        this.isStuffed = isStuffed;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Type{" +
                "candyGroup=" + candyGroup.toString() +
                ", caramel=" + caramel +
                ", iris=" + iris+
                ", cooler=" + chocolate +
                ", isStuffed=" + isStuffed +
                "additionalProperties" + additionalProperties +
                '}';
    }

}