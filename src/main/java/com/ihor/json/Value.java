package com.ihor.json;

import java.util.HashMap;
import java.util.Map;


public class Value {

    private String proteins;
    private CandyGroup_ candyGroup;
    private String fats;
    private String carbohydrates;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Value() {
    }

    /**
     *
     * @param candyGroup
     * @param fats
     * @param carbohydrates
     * @param proteins
     */
    public Value(String proteins, CandyGroup_ candyGroup, String fats, String carbohydrates) {
        super();
        this.proteins = proteins;
        this.candyGroup = candyGroup;
        this.fats = fats;
        this.carbohydrates = carbohydrates;
    }

    public String getProteins() {
        return proteins;
    }

    public void setProteins(String proteins) {
        this.proteins = proteins;
    }

    public CandyGroup_ getCandyGroup() {
        return candyGroup;
    }

    public void setCandyGroup(CandyGroup_ candyGroup) {
        this.candyGroup = candyGroup;
    }

    public String getFats() {
        return fats;
    }

    public void setFats(String fats) {
        this.fats = fats;
    }

    public String getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(String carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "Type{" +
                "candyGroup=" + candyGroup.toString() +
                "proteins=" + proteins +
                ", fats=" + fats +
                ", carbohydrates=" + carbohydrates +
                "additionalProperties" + additionalProperties +
                '}';
    }

}