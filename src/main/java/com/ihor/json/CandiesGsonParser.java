package com.ihor.json;

import com.google.gson.Gson;
import com.ihor.json.CandieValidation;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class CandiesGsonParser {


    public static void parseJson(String path) {
        Gson gson = new Gson();
        Candies candies = null;
        try {
            candies = gson.fromJson(new FileReader(path), Candies.class);
            List<Candy> candyList = (candies.getCandy());
//            candyList.sort(new Candy.SortByCandyId());
            candyList.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}