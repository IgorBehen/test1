package com.ihor.json;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class CandieValidation {

    public static boolean validateJson(String validateSchema, String json) throws FileNotFoundException {
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(new FileReader(validateSchema)));
        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(new FileReader(json)));
        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubject);
        } catch (ValidationException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
