package com.ihor.json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Candies {

    private List<Candy> candy = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Candies() {
    }

    /**
     *
     * @param candy
     */
    public Candies(List<Candy> candy) {
        super();
        this.candy = candy;
    }

    public List<Candy> getCandy() {
        return candy;
    }

    public void setCandy(List<Candy> candy) {
        this.candy = candy;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
     return "CandyGroup{" +
             "candy: " +  candy +
            "additionalProperties" + additionalProperties +
            '}';
    }
}