package com.ihor.json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Comparator;

public class Candy {

    private String candyId;
    private String name;
    private String production;
    private String energy;
    private Type type;
    private Value value;
    private List<String> ingredients = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public Candy() {
    }

    /**
     * @param ingredients
     * @param candyId
     * @param name
     * @param value
     * @param type
     * @param production
     * @param energy
     */
    public Candy(String candyId, String name, String production, String energy, Type type, Value value, List<String> ingredients) {
        super();
        this.candyId = candyId;
        this.name = name;
        this.production = production;
        this.energy = energy;
        this.type = type;
        this.value = value;
        this.ingredients = ingredients;
    }

    public String getCandyId() {
        return candyId;
    }

    public void setCandyId(String candyId) {
        this.candyId = candyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    public String getEnergy() {
        return energy;
    }

    public void setEnergy(String energy) {
        this.energy = energy;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    @Override
    public String toString() {
        return "Candie{" +
                "candyId=" + candyId +
                ", name='" + name + '\'' +
                ", production='" + production + '\'' +
                ", value=" + value +
                ", type=" + type +
                ", energy=" + energy +
                ", ingredients=" + ingredients +
                "ingredients: " + ingredients +
                "additionalProperties" + additionalProperties +
                "}\n";
    }

}