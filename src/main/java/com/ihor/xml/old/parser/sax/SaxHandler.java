package com.ihor.xml.old.parser.sax;//package com.ihor.xml.old.parser.sax;
//
//import com.lviv.hv.Model.Device;
//import com.lviv.hv.Model.DeviceGroup;
//import com.lviv.hv.Model.Port;
//import com.lviv.hv.Model.Type;
//import org.xml.sax.Attributes;
//import org.xml.sax.SAXException;
//import org.xml.sax.helpers.DefaultHandler;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class SaxHandler extends DefaultHandler {
//
//  static final String DEVICE_TAG = "device";
//  static final String NAME_TAG = "name";
//  static final String ORIGIN_TAG = "origin";
//  static final String PRICE_TAG = "price";
//  static final String CRITICAL_TAG = "critical";
//  static final String TYPES_TAG = "types";
//  static final String PERIPHERAL_TAG = "peripheral";
//  static final String DEVICE_GROUP_TAG = "deviceGroup";
//  static final String ENERGY_CONSUMPTION_TAG = "energyConsumption";
//  static final String COOLER_TAG = "cooler";
//  static final String PORTS_TAG = "ports";
//  static final String PORT_TAG = "port";
//
//  static final String DEVICE_ID_ATTRIBUTE = "deviceId";
//  static final String DEVICE_GROUP_ID_ATTRIBUTE = "id";
//
//  private List<Device> devices = new ArrayList<>();
//  private Device device;
//  private Type type;
//  private DeviceGroup deviceGroup;
//  private List<Port> ports;
//  private String currentElement;
//
//  public List<Device> getDeviceList() {
//    return this.devices;
//  }
//
//  @Override
//  public void startElement(String uri, String localName, String qName, Attributes attributes)
//      throws SAXException {
//    currentElement = qName;
//
//    switch (currentElement) {
//      case DEVICE_TAG: {
//        String deviceId = attributes.getValue(DEVICE_ID_ATTRIBUTE);
//        device = new Device();
//        device.setDeviceId(Integer.parseInt(deviceId));
//        break;
//      }
//      case TYPES_TAG: {
//        type = new Type();
//        break;
//      }
//      case DEVICE_GROUP_TAG: {
//        String deviceGroupId = attributes.getValue(DEVICE_GROUP_ID_ATTRIBUTE);
//        deviceGroup = new DeviceGroup();
//        deviceGroup.setId(Integer.parseInt(deviceGroupId));
//        break;
//      }
//      case PORTS_TAG: {
//        ports = new ArrayList<>();
//        break;
//      }
//    }
//  }
//
//  @Override
//  public void endElement(String uri, String localName, String qName) throws SAXException {
//    switch (qName) {
//      case DEVICE_TAG: {
//        devices.add(device);
//        break;
//      }
//      case TYPES_TAG: {
//        device.setType(type);
//        type = null;
//        break;
//      }
//      case DEVICE_GROUP_TAG: {
//        type.setDeviceGroup(deviceGroup);
//        deviceGroup = null;
//        break;
//      }
//      case PORTS_TAG: {
//        type.setPorts(ports);
//        ports = null;
//        break;
//      }
//    }
//  }
//
//  @Override
//  public void characters(char[] ch, int start, int length) throws SAXException {
//    if (currentElement.equals(NAME_TAG)) {
//      device.setName(new String(ch, start, length));
//    }
//    if (currentElement.equals(ORIGIN_TAG)) {
//      device.setOrigin(new String(ch, start, length));
//    }
//    if (currentElement.equals(PRICE_TAG)) {
//      device.setPrice(Integer.parseInt(new String(ch, start, length)));
//    }
//    if (currentElement.equals(CRITICAL_TAG)) {
//      device.setCritical(Boolean.parseBoolean(new String(ch, start, length)));
//    }
//    if (currentElement.equals(PERIPHERAL_TAG)) {
//      type.setPeripheral(Boolean.parseBoolean(new String(ch, start, length)));
//    }
//    if (currentElement.equals(DEVICE_GROUP_TAG)) {
//      deviceGroup.setName(new String(ch, start, length));
//    }
//    if (currentElement.equals(ENERGY_CONSUMPTION_TAG)) {
//      type.setEnergyConsumption(Integer.parseInt(new String(ch, start, length)));
//    }
//    if (currentElement.equals(COOLER_TAG)) {
//      type.setCooler(Boolean.parseBoolean(new String(ch, start, length)));
//    }
//    if (currentElement.equals(PORT_TAG)) {
//      Port port = new Port(new String(ch, start, length));
//      ports.add(port);
//    }
//  }
//}
