package com.ihor.xml.old.parser.dom;//package com.ihor.xml.old.parser.dom;
//
//import com.lviv.hv.Model.Device;
//import com.lviv.hv.xmlValidator.XmlValidator;
//import org.w3c.dom.Document;
//import org.xml.sax.SAXException;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//
//public class Main {
//
//  private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//  private static DocumentBuilder documentBuilder;
//
//  public static void main(String... args) {
//    File xmlFile = new File("src\\main\\resources\\xml\\devices.xml");
//    File xsdFile = new File("src\\main\\resources\\xml\\devicesXSD.xsd");
//
//    Document document = null;
//    try {
//      documentBuilder = factory.newDocumentBuilder();
//      document = documentBuilder.parse(xmlFile);
//    } catch (ParserConfigurationException | SAXException | IOException e) {
//      e.printStackTrace();
//    }
//
//    if (XmlValidator.validate(document, xsdFile)) {
//      List<Device> deviceList = DOMParser.parse(document);
//      System.out.println(deviceList);
//    } else {
//      System.out.println("XML document failed validation.");
//    }
//  }
//
//}
