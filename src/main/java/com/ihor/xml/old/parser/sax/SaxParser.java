package com.ihor.xml.old.parser.sax;//package com.ihor.xml.old.parser.sax;
//
//import com.lviv.hv.Model.Device;
//import com.lviv.hv.xmlValidator.XmlValidator;
//import org.xml.sax.SAXException;
//
//import javax.xml.parsers.ParserConfigurationException;
//import javax.xml.parsers.SAXParser;
//import javax.xml.parsers.SAXParserFactory;
//import java.io.File;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//
//public class SaxParser {
//
//  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
//
//  public static List<Device> parse(File xml, File xsd) {
//    List<Device> devices = new ArrayList<>();
//    try {
//      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
//      saxParserFactory.setValidating(true);
//
//      SAXParser saxParser = saxParserFactory.newSAXParser();
//      System.out.println(saxParser.isValidating());
//      SaxHandler saxHandler = new SaxHandler();
//      saxParser.parse(xml, saxHandler);
//      devices = saxHandler.getDeviceList();
//    } catch (SAXException | ParserConfigurationException | IOException ex) {
//      ex.printStackTrace();
//    }
//    return devices;
//  }
//}