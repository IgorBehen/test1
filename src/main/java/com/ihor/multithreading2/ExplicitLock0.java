package com.ihor.multithreading2;//package com.ihor.multithreading2.explicit_lock;
//
//import java.time.LocalDateTime;
//import java.util.concurrent.locks.Lock;
//import java.util.concurrent.locks.ReentrantReadWriteLock;
//
//public class ExplicitLock {
//
//    private static int task = 0 ;
//    private ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
//    private Lock rLock = rwLock.readLock();
//    private Lock wLock = rwLock.writeLock();
//
//    public ExplicitLock(int task) {
//        this.task = task;
//    }
//
//    public int getValue() {
//        rLock.lock();
//        try {
//            return this.task;
//        } finally {
//            rLock.unlock();
//        }
//    }
//
//    public void setValue(int task) {
//        wLock.lock();
//        try {
//            this.task = task;
//        } finally {
//            wLock.unlock();
//        }
//    }
//}
//
//
//
//
//    static class SynchMeth implements Runnable {
//        int counter;
//        int n = 1;
//
//        @Override
//        public void run() {
//            while (counter < 20) {
//                synchronized (lock) {
//                    counter++;
//                    String trdName = Thread.currentThread().getName();
//                    try {
//                        Thread.sleep(500);
//                        System.out.println("Thread name: " + trdName + " ### " + n++ + " times" + " --> "
//                                + LocalDateTime.now());
//                        ExplicitLock.task += 1;
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//
//        }
//    }
//
//    public static void main(String[] args) {
//        Thread t1 = new Thread(new com.ihor.multithreading.three_synchronized_methods.ThreeSynchronizedMethods.SynchMeth());
//        Thread t2 = new Thread(new com.ihor.multithreading.three_synchronized_methods.ThreeSynchronizedMethods.SynchMeth());
//        Thread t3 = new Thread(new com.ihor.multithreading.three_synchronized_methods.ThreeSynchronizedMethods.SynchMeth());
//        t1.start();
//        t2.start();
//        t3.start();
//        try {
//            t1.join();
//            t2.join();
//            t3.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("task = " + task);
//    }
//}