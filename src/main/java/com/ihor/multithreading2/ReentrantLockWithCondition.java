package com.ihor.multithreading2;

import java.time.LocalDateTime;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockWithCondition {
    static Stack<String> stack = new Stack<>();
    final static int CAPACITY = 2;
    static ReentrantLock lock = new ReentrantLock();
    static Condition stackEmptyCondition = lock.newCondition();
    static Condition stackFullCondition = lock.newCondition();
    public static void pushToStack(String item) {
        try {
            lock.lock();
            while (stack.size() == CAPACITY) {
                System.out.println("Stack is full " + LocalDateTime.now());
                stackFullCondition.await();
            }
            stack.push(item);
            System.out.println("Stack <- " + item + " " + LocalDateTime.now());
            stackEmptyCondition.signalAll();
        } catch (InterruptedException e) { } finally {
            lock.unlock();
        } }

    public static String popFromStack() {
        String result = "";
        try {
            lock.lock();
            while (stack.size() == 0) {
                System.out.println("Stack is empty " + LocalDateTime.now());
                stackEmptyCondition.await();
            }
            result = stack.pop();
        } catch (InterruptedException e) { } finally {
            stackFullCondition.signalAll();
            lock.unlock();
        }
        System.out.println("Stack -> " + result + " " + LocalDateTime.now());
        return result;
    }
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(() -> {
            pushToStack("value=1");
            try { Thread.sleep(1000); } catch (InterruptedException e) { }
            pushToStack("value=2");
            try { Thread.sleep(1000); } catch (InterruptedException e) { }
            pushToStack("value=3");
            try { Thread.sleep(2000); } catch (InterruptedException e) { }
            pushToStack("value=4"); });
        executor.submit(() -> {
            try { Thread.sleep(3000); } catch (InterruptedException e) { }
            popFromStack();
            popFromStack();
            popFromStack();
            popFromStack(); });
    }
}