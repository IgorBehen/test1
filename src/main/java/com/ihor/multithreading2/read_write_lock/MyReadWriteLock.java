package com.ihor.multithreading2.read_write_lock;//package com.ihor.multithreading2.read_write_lock;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.util.concurrent.locks.Lock;
//import java.util.concurrent.locks.ReadWriteLock;
//
//public interface MyReadWriteLock extends ReadWriteLock {
//    @NotNull
//    Lock readLock();
//    @NotNull
//    Lock writeLock();
//}
//
