package com.ihor.multithreading2.explicit_lock;

import java.time.LocalDateTime;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class NumberCount {
    private int task = 0;
    private Lock lock = new ReentrantLock();

    int getBalance() {
        return task;
    }

    void counting() {
        int counter = 0;
        int n = 1;
        lock.lock();
        try {
            while (counter < 20) {
                counter++;
                String trdName = Thread.currentThread().getName();
                Thread.sleep(500);
                System.out.println("Thread name: " + trdName + " ### " + n++ + " times" + " --> "
                        + LocalDateTime.now());
                task++;
            }
        } catch (InterruptedException ex) {
        } finally {
            lock.unlock();
        }
    }
}

class AddAmountTask implements Runnable {
    NumberCount nc;

    AddAmountTask(NumberCount ncc) {
        nc = ncc;
    }

    public void run() {
            nc.counting();
    }
}

public class ExplicitLock {

    public static void main(String[] args) {
        NumberCount nc = new NumberCount();
        AddAmountTask t = new AddAmountTask(nc);
        Thread t1 = new Thread(t);
        Thread t2 = new Thread(t);
        Thread t3 = new Thread(t);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Task = " + nc.getBalance());
    }
}


