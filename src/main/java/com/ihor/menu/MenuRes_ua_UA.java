package com.ihor.menu;

import java.util.ListResourceBundle;

public class MenuRes_ua_UA extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {

        return resources;
    }

    private final Object[][] resources = {

            { "Меню:", "1. Перший пункт меню, тисни           - 1\n" +
                       "2. Другий пункт меню, тисни           - 2\n" +
                       "3. Третій  пункт меню, тисни          - 3\n" +
                       "4. Четвертий пункт меню, тисни        - 4\n" +
                       "5. П'ятий  пункт меню, тисни          - 5\n" +
                       "6. Шостий пункт меню, тисни           - 6\n" },
    };

}




