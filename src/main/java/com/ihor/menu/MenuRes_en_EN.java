package com.ihor.menu;

import java.util.ListResourceBundle;

public class MenuRes_en_EN extends ListResourceBundle{

        @Override
        protected Object[][] getContents() {

            return resources;
        }

        private final Object[][] resources = {

                { "Menu:", "1. The first menu item, press         - 1\n" +
                           "2. The second menu item, press        - 2\n" +
                           "3. The third menu item, press         - 3\n" +
                           "4. The fourth item in the menu, press - 4\n" +
                           "5. Fifth menu item, press             - 5\n" +
                           "6. Sixth menu item, press             - 6\n"    },

        };

}
