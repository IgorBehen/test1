package com.ihor.menu;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;


public class MenuResBundle {

    void localeEN() {
        Locale en_loc = new Locale("en", "EN");
        ResourceBundle bundle =
                ResourceBundle.getBundle("com.ihor.menu.MenuRes_en_EN", en_loc);

        System.out.println("Menu: \n" + bundle.getObject("Menu:"));

    }

    void localeUA() {
        Locale ua_loc = new Locale("ua", "UA");
        ResourceBundle bundle2 =
                ResourceBundle.getBundle("com.ihor.menu.MenuRes_ua_UA", ua_loc);

        System.out.println("Меню: \n" + bundle2.getObject("Меню:"));

        System.out.println();
    }

    public static void main(String[] args) {
        MenuResBundle mrb = new MenuResBundle();
        Scanner sc = new Scanner(System.in);
        int number;
        boolean quit = false;
        while (!quit) {
            System.out.print("Choose menu item: \n" +
                             "Press 1 to English. \n" +
                             "Press 2 to Ukrainian. \n" +
                             "Press 0 for Quit. \n");
            number = sc.nextInt();

            switch (number) {
                case 1:
                    mrb.localeEN();
                    break;
                case 2:
                    mrb.localeUA();
                    break;
                case 0:
                    quit = true;
                    break;
                default:
                    System.out.println("Invalid choice.");
            }
        }

    }
}