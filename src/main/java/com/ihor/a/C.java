package com.ihor.a;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ihor Behen
 */
public class C {
    public static void main(String[] args) {
        /**
         * @some logic
         */

        Integer x = new Integer(22);
        System.out.println(x instanceof Integer);

        Cat cat = new Cat();
        System.out.println(cat instanceof Animal);
        System.out.println(cat instanceof MaineCoon);
        Class<?> cl = cat.getClass();
        MaineCoon mc = new MaineCoon();
        Class<?> cl1 = mc.getClass();
        System.out.println(cl1);
        System.out.println(mc instanceof Cat);
        double [] arr = new double[10];
        Class<?> cl2 = arr.getClass();
        System.out.println(cl2);
        Class<?> cl3 = "name".getClass();
        System.out.println(cl3);
        Class<?> cl4 = Integer.valueOf(5).getClass();
        System.out.println(cl4);
        C c = new C();
        Class<?> cl5 = c.getClass();
        System.out.println(cl5);

        List<Integer> list = new ArrayList<>();
        list.add(5);
        list.add(9);
        show(list);
    }

    /**
     * Method show
     * @param list
     */
   static  void show(List list){
        list.forEach(System.out::println);
    }
}

/**
 * Class Animal
 */
class Animal {

}

/**
 * Class Cat extends Class Animal
 */
class Cat extends Animal {

}

/**
 * Class MainCoon extends Class Cat
 */
class MaineCoon extends Cat {

}