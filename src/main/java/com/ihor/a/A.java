package com.ihor.a;

        import java.util.LinkedList;
        import java.util.List;

public class A {

    List<String> availablesBouquets;

     {
        availablesBouquets = new LinkedList<String>();
        availablesBouquets.add("BouquetCatalog.BOUQUET1");  // BOUQUET1 ... -треба замінити на реальні значення ENUM-а
        availablesBouquets.add("BouquetCatalog.BOUQUET3");
        availablesBouquets.add("BouquetCatalog.BOUQUET5");
        availablesBouquets.add("BouquetCatalog.BOUQUET7");
        availablesBouquets.add("BouquetCatalog.BOUQUET9");
    }

    public static void main(String[] args) {
        A a = new A();
        System.out.println(a);
        int i = 1;
        for (String b : a.availablesBouquets) {
            System.out.println(i + ". " + b);
            i++;
        }

    }
}