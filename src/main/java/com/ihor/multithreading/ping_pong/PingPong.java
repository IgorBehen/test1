package com.ihor.multithreading.ping_pong;

public class PingPong {
    static Object lock = new Object();
    static boolean isOk = true;

    static class Game implements Runnable{
        int counter;

        @Override
        public void run() {
            while (counter < 20){
                synchronized(lock){
                    counter++;
                    String trdName = Thread.currentThread().getName();
                    try {
                        if(isOk) {
                            Thread.sleep(1000);
                            System.out.println(trdName + ":   Ping");
                            isOk = !isOk;
                            lock.notify();
                            lock.wait();

                        } else {
                            Thread.sleep(1000);
                            System.out.println(trdName + ":   Pong");
                            isOk = !isOk;
                            lock.notify();
                            lock.wait();
                        }
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static void main(String[] args){
        Thread t1 = new Thread(new Game());
        Thread t2 = new Thread(new Game());
        t1.start();
        t2.start();
    }
}