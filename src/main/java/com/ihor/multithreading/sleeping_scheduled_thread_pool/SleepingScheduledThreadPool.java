package com.ihor.multithreading.sleeping_scheduled_thread_pool;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.*;

public class SleepingScheduledThreadPool {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int randomNumberInRange;
        int numSeconds;
        SleepingScheduledThreadPool sleepingScheduledThreadPool = new SleepingScheduledThreadPool();
        int quantity = sleepingScheduledThreadPool.enteringNumber();

        for (int i = 0; i < quantity; i++) {
            randomNumberInRange = sleepingScheduledThreadPool.findRandomNumberInRange(1, 10);
            numSeconds = randomNumberInRange * 1000;

            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
            Runnable task = () -> System.out.println("\nThe thread is sleeping...");
            ScheduledFuture<?> future = executor.schedule(task, 500, TimeUnit.MILLISECONDS);
            TimeUnit.MILLISECONDS.sleep(numSeconds);
            System.out.printf("The thread slept for %sms", numSeconds);
            executor.shutdown();
        }
    }

    private int findRandomNumberInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private int enteringNumber() {
        System.out.println("Enter the number of times the stream will sleep: ");
        Scanner sc;
        boolean b = true;
        int quantity1 = 0;
        while (b) {
            try {
                sc = new Scanner(System.in);
                quantity1 = sc.nextInt();
                b = false;
            } catch (Exception e) {
                System.out.println("Please enter a valid value!");
                b = true;
            }
        }
        return quantity1;
    }
}
