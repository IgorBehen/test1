package com.ihor.multithreading.pipes;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.atomic.AtomicInteger;

public class PipesToCommunicate {
    public static void main(String[] args) throws IOException, InterruptedException {
        AtomicInteger num = new AtomicInteger();
        final PipedInputStream pipedInputStream = new PipedInputStream();
        final PipedOutputStream pipedOutputStream = new PipedOutputStream();
        pipedInputStream.connect(pipedOutputStream);

        Thread pipeWriter = new Thread(() -> {
            for (int i = 1; i < 50; i++) {
                try {
                    pipedOutputStream.write(num.addAndGet(10));
                    Thread.sleep(500);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread pipeReader = new Thread(() -> {
            for (int i = 1; i < 50; i++) {
                try {
                    System.out.print((char) pipedInputStream.read());
                    Thread.sleep(1000);
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
        pipeWriter.start();
        pipeReader.start();
        pipeWriter.join();
        pipeReader.join();
        pipedOutputStream.close();
        pipedInputStream.close();
    }
}
