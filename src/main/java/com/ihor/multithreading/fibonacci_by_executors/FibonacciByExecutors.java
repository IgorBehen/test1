package com.ihor.multithreading.fibonacci_by_executors;

import java.util.*;
import java.util.concurrent.*;

class FibonacciByExecutors {
    private static long n0 = 1;
    private static long n1 = 1;
    private static long n2;
    private int quantity = 0;


    private FibonacciByExecutors() {
    }

    private FibonacciByExecutors(int quantity) {
        this.quantity = quantity;
    }

    private int enteringNumber() {
        System.out.println("Enter the quantity of Fibonacci numbers: "
                + "\n The sequence, will be the first two digits of the Fibonacci sequence(1, 1...) " +
                "and the quantity of digits you entered.");
        Scanner sc;
        boolean b = true;
        int quantity1 = 0;
        while (b) {
            try {
                sc = new Scanner(System.in);
                quantity1 = sc.nextInt();
                b = false;
            } catch (Exception e) {
                System.out.println("Please enter a valid value!");
                b = true;
            }
        }
        return quantity1;
    }

    public static void main(String[] args) throws InputMismatchException, InterruptedException {
        FibonacciByExecutors competition = new FibonacciByExecutors();
        int quantity1 = competition.enteringNumber();
        System.out.println("You have entered the number: " + quantity1);
        FibonacciByExecutors competition1 = new FibonacciByExecutors(quantity1);

        // 3 methods of calculation
        competition1.byExecutorService();
//        competition1.byExecutorServiceWithFuture();
//        competition1.byExecutorServiceWithFutureWithInvoke();
    }

    private void byExecutorService() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> System.out.print(n0 + ", " + n1 + ", "));
        executorService.submit(() ->
        {
            for (int i = 1; i <= quantity; i++) {
                n2 = n0 + n1;
                System.out.print(n2 + ", ");
                n0 = n1;
                n1 = n2;
            }
        });
        executorService.shutdown();
    }

    private void byExecutorServiceWithFuture() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future futureA = executorService.submit(() -> System.out.print(n0 + ", " + n1 + ", "));
        Future<Long> futureB = executorService.submit(() -> {
            for (int i = 1; i <= quantity; i++) {
                n2 = n0 + n1;
                System.out.print(n2 + ", ");
                n0 = n1;
                n1 = n2;
            }
            return n2;
        });
        try {
            System.out.println("\nThe largest number of the specified number in the sequence is - " + futureB.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }

    private void byExecutorServiceWithFutureWithInvoke() throws InterruptedException {
        String str = firstNumbers();
        String str1 = sequence();
        String str2 = lastNumber();
        ExecutorService executor = Executors.newWorkStealingPool();
        List<Callable<String>> callables = Arrays.asList(() -> str, () -> str1, () -> str2);
        executor.invokeAll(callables)
                .stream().map(future -> {
            try {
                return future.get();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        })
                .forEach(System.out::print);
    }

    private String firstNumbers() {
        return n0 + ", " + n1 + ", ";
    }

    private String sequence() {
        List<Long> list = new ArrayList<>();
        for (int i = 1; i <= quantity; i++) {
            n2 = n0 + n1;
            list.add(n2);
            n0 = n1;
            n1 = n2;
        }
        StringBuilder strbul = new StringBuilder();
        Iterator<Long> iter = list.iterator();
        while (iter.hasNext()) {
            strbul.append(iter.next());
            if (iter.hasNext()) {
                strbul.append(", ");
            }
        }
        return strbul.toString();
    }

    private String lastNumber() {
        return "\nThe largest number of the specified number in the sequence is - " + n2;
    }
}

