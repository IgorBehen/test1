package com.ihor.multithreading.__different__;

import java.time.LocalDateTime;

public class A {

    private static long A = 0; // Shared Resource
    private static Object sync = new Object(); // Monitor
    class MyThread extends Thread {
        @Override
        public void run() {
            for (int i = 1; i <= 1000000000; i++) { synchronized (sync){ A++; } }
            System.out.println("finish " + Thread.currentThread().getName());
        } }
    public void show() {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        MyThread t3 = new MyThread();
        t1.start(); t2.start(); t3.start();
        try {
            t1.join(); t2.join(); t3.join();
        } catch (InterruptedException e) { e.printStackTrace(); }
        System.out.println("A=" + A);
    }

    public static void main(String[] args) {
        A a = new A();
        long start = System.currentTimeMillis();
        System.out.println("Full time - start: " + LocalDateTime.now());
        a.show();
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
        float min = (end - start) /(60*1000F);
        System.out.println(min + " minutes");
        System.out.println("Full time - finish: " + LocalDateTime.now());
    }
}
