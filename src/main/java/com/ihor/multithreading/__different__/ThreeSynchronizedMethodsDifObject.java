package com.ihor.multithreading.__different__;



import java.time.LocalDateTime;

public class ThreeSynchronizedMethodsDifObject {

    private static final Object lock = new Object();
    static int task = 0;

    static class SynchMeth implements Runnable{
        int counter;
        int n = 1;
        @Override
        public void run() {
            while (counter < 20){
                synchronized(lock){
                    counter++;
                    String trdName = Thread.currentThread().getName();
                    try {
                        //System.out.println(" /"+ n++ + "/ ");
                        Thread.sleep(500);
                        System.out.println("Thread name: " + trdName + " ### " + n++ + " times" + " --> "
                                + LocalDateTime.now());
                        task += 1;
//                        lock.notify();
//                        lock.wait();
                    } catch(InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public static void main(String[] args){
        Thread t1 = new Thread(new SynchMeth());
        Thread t2 = new Thread(new SynchMeth());
        Thread t3 = new Thread(new SynchMeth());
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("task = " + task);
    }

}




//    private int count = 0;
//    private static Object obj = new Object();
//    public void myMethod() {
//// code ...
//        synchronized (obj) { // code ... }
//// code ...
//        }
//        public void otherMethod() {
//// code ...
//            synchronized (obj) { // code ... }
//// code ...
//            }




//import java.util.ArrayList;
//import java.util.List;
//
//class ThreeSynchronizedMethodsDifObject {
//    public static void main(String... args) {
//        List<Thread> threads = new ArrayList<Thread>();
//
//        System.out.println("----- First Test, static method -----");
//        for (int i = 0; i < 4; ++i) {
//            threads.add(new Thread(() -> {
//                ThreeSynchronizedMethodsDifObject.m1();
//            }));
//        }
//        for (Thread t : threads) {
//            t.start();
//        }
//        for (Thread t : threads) {
//            try {
//                t.join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        System.out.println("----- Second Test, non-static method -----");
//        threads.clear();
//        for (int i = 0; i < 4; ++i) {
//            threads.add(new Thread(() -> {
//                new ThreeSynchronizedMethodsDifObject().m2();
//            }));
//        }
//        for (Thread t : threads) {
//            t.start();
//        }
//        for (Thread t : threads) {
//            try {
//                t.join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        System.out.println("----- Third Test, non-static method, same object -----");
//        threads.clear();
//        final ThreeSynchronizedMethodsDifObject m = new ThreeSynchronizedMethodsDifObject();
//        for (int i = 0; i < 4; ++i) {
//            threads.add(new Thread(() -> {
//                m.m2();
//            }));
//        }
//        for (Thread t : threads) {
//            t.start();
//        }
//        for (Thread t : threads) {
//            try {
//                t.join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//
//    public static synchronized void m1() {
//        System.out.println(Thread.currentThread() + ": starting.");
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(Thread.currentThread() + ": stopping.");
//    }
//
//    public synchronized void m2() {
//        System.out.println(Thread.currentThread() + ": starting.");
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(Thread.currentThread() + ": stopping.");
//    }
//}
