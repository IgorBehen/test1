package com.ihor.multithreading.__different__;

class NeToy_PingPong extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            try {
                sleep(1500);
            } catch (InterruptedException e) {
            }
            System.out.println("Ping!");
        }
    }
}


public class MultiThreading {
    static NeToy_PingPong pingPong;

    public static void main(String[] args) {
        pingPong = new NeToy_PingPong();
        System.out.println("Start game...");
        pingPong.start();
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
            }
            System.out.println("Pong!");
        }
        if (pingPong.isAlive()) {
            try {
                pingPong.join();
            } catch (InterruptedException e) {
            }
            System.out.println("Ping won!");
        } else {
            System.out.println("Pong won!");
        }
        System.out.println("Game over!");
    }
}
