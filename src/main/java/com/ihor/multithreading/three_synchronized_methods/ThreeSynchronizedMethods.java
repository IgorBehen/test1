package com.ihor.multithreading.three_synchronized_methods;

import java.time.LocalDateTime;

public class ThreeSynchronizedMethods {
    private static final Object lock = new Object();
    private static int task = 0;

    static class SynchMeth implements Runnable {
        int counter;
        int n = 1;

        @Override
        public void run() {
            while (counter < 20) {
                synchronized (lock) {
                    counter++;
                    String trdName = Thread.currentThread().getName();
                    try {
                        Thread.sleep(500);
                        System.out.println("Thread name: " + trdName + " ### " + n++ + " times" + " --> "
                                + LocalDateTime.now());
                        task += 1;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
    }

    public static void main(String[] args) {
        Thread t1 = new Thread(new SynchMeth());
        Thread t2 = new Thread(new SynchMeth());
        Thread t3 = new Thread(new SynchMeth());
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("task = " + task);
    }
}
