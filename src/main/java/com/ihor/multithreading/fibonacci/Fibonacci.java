package com.ihor.multithreading.fibonacci;

import java.util.InputMismatchException;
import java.util.Scanner;

class Fibonacci {
    private static long n0 = 1;
    private static long n1 = 1;
    private static long n2;
    private static final Object lock = new Object();
    private int quantity = 0;
    private Fibonacci(){
    }

    private Fibonacci(int quantity){
        this.quantity = quantity;
    }

    class MyThread extends Thread {

        @Override
        public void run() {
            for (int i = 1; i <= quantity/3; i++) {
                synchronized (lock) {
                    n2 = n0 + n1;
                    System.out.print(n2 + ", ");
                    n0 = n1;
                    n1 = n2;
                }
            }
        }
    }

    public void show() {
        MyThread t1 = new MyThread();
        MyThread t2 = new MyThread();
        MyThread t3 = new MyThread();
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private int enteringNumber(){
        System.out.println("Enter the quantity of Fibonacci numbers: "
                + "\n The sequence, will be the first two digits of the Fibonacci sequence(1, 1...) " +
                "and the quantity of digits you entered.\"");
        Scanner sc;
        boolean b = true;
        int quantity1 = 0;
        while(b) {
            try {
                sc = new Scanner(System.in);
                quantity1 = sc.nextInt();
                b = false;
            } catch (Exception e) {
                System.out.println("Please enter a valid value!");
                b = true;
            }
        }
        return quantity1;
    }
    public static void main(String[] args) throws InputMismatchException{
        Fibonacci competition = new Fibonacci();
        int quantity1 = competition.enteringNumber();
        System.out.println("You have entered the number: " + quantity1);
        Fibonacci competition1 = new Fibonacci(quantity1);
        System.out.print(n0 + ", " + n1 + ", ");
        competition1.show();
    }
}
