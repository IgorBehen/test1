package com.ihor.multithreading.a;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;

/**
 * !!!!!!!!!!!!!!!!!�� ��, ������������!!!!!!!!!
 */
public class PipeCommunicate  {

    private static final Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        PipeCommunicate pc = new PipeCommunicate();
        pc.start("some text");
    }

    public void start(final String text) {
        final PipedInputStream pipedInputStream = new PipedInputStream();
        final PipedOutputStream pipedOutputStream = new PipedOutputStream();
        try {
            pipedInputStream.connect(pipedOutputStream);
        } catch (IOException e) {
            logger.error(e);
        }
        Thread pipeWriter = pipeWrite(pipedOutputStream, text);
        Thread pipeReader = pipeRead(pipedInputStream);
        pipeWriter.start();
        pipeReader.start();
        try {
            pipeWriter.join();
            pipeReader.join();
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    private Thread pipeWrite(PipedOutputStream pipedOutputStream, String text) {
        return new Thread(() -> {
            try {
                for (int ch : text.toCharArray()) {
                    pipedOutputStream.write(ch);
                }
                pipedOutputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        });
    }

    private Thread pipeRead(PipedInputStream pipedInputStream){
        return new Thread(() -> {
            int ch;
            try {
                while ((ch = pipedInputStream.read()) != -1) {
                    System.out.print((char)ch);
                    Thread.sleep(500);
                }
                pipedInputStream.close();
            } catch (IOException | InterruptedException e) {
                logger.error(e);
            }
        });
    }

}
