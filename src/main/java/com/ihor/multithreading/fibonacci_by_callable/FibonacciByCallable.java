package com.ihor.multithreading.fibonacci_by_callable;

import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibonacciByCallable {
    private static long n0 = 1;
    private static long n1 = 1;
    private static long n2;
    private static long sum = 0;
    private int quantity = 0;

    private FibonacciByCallable() {
    }

    private FibonacciByCallable(int quantity) {
        this.quantity = quantity;
    }

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        FibonacciByCallable competition = new FibonacciByCallable();
        int quantity1 = competition.enteringNumber();
        System.out.println("You have entered the number: " + quantity1);
        FibonacciByCallable competition1 = new FibonacciByCallable(quantity1);
        competition1.fibonacciSumCallable();
    }

    void fibonacciSumCallable() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Callable<Long> callable = () -> {
            for (int i = 1; i <= quantity; i++) {
                n2 = n0 + n1;
                System.out.print(n2 + ", ");
                n0 = n1;
                n1 = n2;
                sum += n2;
            }
            return sum;
        };
        Future<Long> future = executorService.submit(callable);
        System.out.print(n0 + ", " + n1 + ", ");
        Long result = future.get();
        System.out.println("\nThe sum of Fibonacci numbers is: " + (result + 2));
        executorService.shutdown();
    }

    private int enteringNumber() {
        System.out.println("Enter the quantity of Fibonacci numbers: "
                + "\n The sequence, will be the first two digits of the Fibonacci sequence(1, 1...) " +
                "and the quantity of digits you entered.\"");
        Scanner sc;
        boolean b = true;
        int quantity1 = 0;
        while (b) {
            try {
                sc = new Scanner(System.in);
                quantity1 = sc.nextInt();
                b = false;
            } catch (Exception e) {
                System.out.println("Please enter a valid value!");
                b = true;
            }
        }
        return quantity1;
    }
}
