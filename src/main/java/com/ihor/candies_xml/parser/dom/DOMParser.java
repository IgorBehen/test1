package com.ihor.candies_xml.parser.dom;

import com.ihor.candies_xml.model.Candy;
import com.ihor.candies_xml.model.Ingredients;
import com.ihor.candies_xml.model.Type;
import com.ihor.candies_xml.model.Value;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class DOMParser {

    public static List<Candy> parse(Document document) {
        List<Candy> candies = new ArrayList<>();
        NodeList nodeList = document.getElementsByTagName("candy");
        for (int index = 0; index < nodeList.getLength(); index++) {
            Candy candy = new Candy();
            Value value;
            Type type;
            List<Ingredients> ingredients;
            Node node = nodeList.item(index);
            Element element = (Element) node;
            candy.setCandieId(Integer.parseInt(element.getAttribute("candieId")));
            candy.setName(element.getElementsByTagName("name").item(0).getTextContent());
            candy.setProduction(element.getElementsByTagName("production").item(0).getTextContent());
            candy.setEnergy(Integer.parseInt(element.getElementsByTagName("energy").item(0)
                    .getTextContent()));

            value = getValue(element.getElementsByTagName("value"));
            candy.setValue(value);

            type = getType(element.getElementsByTagName("type"));
            candy.setType(type);

//            ingredients = getIngredients(element.getElementsByTagName("ingredients"));
//            candy.setIngredients((Ingredients) ingredients);

            candies.add(candy);
        }
        return candies;
    }

    private static Type getType(NodeList nodeList) {
        Type type = new Type();


        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);
            type.setCaramel(Boolean.parseBoolean(element.getElementsByTagName("caramel").item(0)
                    .getTextContent()));
            type.setIris(Boolean.parseBoolean(element.getElementsByTagName("iris").item(0)
                    .getTextContent()));
            type.setChocolate(Boolean.parseBoolean(element.getElementsByTagName("chocolate").item(0)
                    .getTextContent()));
            type.setIsStuffed(Boolean.parseBoolean(element.getElementsByTagName("isStuffed").item(0)
                    .getTextContent()));
        }
        return type;
    }

    private static Value getValue(NodeList nodeList) {
        Value value = new Value();
        if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) nodeList.item(0);

            value.setProtein(Integer.parseInt(element.getElementsByTagName("protein")
                    .item(0).getTextContent()));
            value.setFat(Integer.parseInt(element.getElementsByTagName("fat")
                    .item(0).getTextContent()));
            value.setCarbohydrate(Integer.parseInt(element.getElementsByTagName("carbohydrate")
                    .item(0).getTextContent()));
        }
        return value;
    }

//    private static List<Ingredients> getIngredients(NodeList nodes) {
//        List<Ingredients> ingredients = new ArrayList<>();
//        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
//            Element element = (Element) nodes.item(0);
//            NodeList nodeList = element.getChildNodes();
//            for (int i = 0; i < nodeList.getLength(); i++) {
//                Node node = nodeList.item(i);
//                if (node.getNodeType() == Node.ELEMENT_NODE) {
//                    Element el = (Element) node;
//                    ingredients.add(new Ingredients(el.getTextContent()));
//                }
//            }
//        }
//        return ingredients;
//    }
}
