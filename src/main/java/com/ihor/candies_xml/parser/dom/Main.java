package com.ihor.candies_xml.parser.dom;

import com.ihor.candies_xml.model.Candies;
import com.ihor.candies_xml.model.Candy;
import com.ihor.candies_xml.xmlValidator.XmlValidator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class  Main {
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    private static DocumentBuilder documentBuilder;

    public static void main(String... args) {
        File xmlFile = new File("src\\main\\resources\\candies.xml");
        File xsdFile = new File("src\\main\\resources\\candiesXSD.xsd");

        Document document = null;
        try {
            documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.parse(xmlFile);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

//        if (XmlValidator.validate(document, xsdFile)) {
            List<Candy> candieList = DOMParser.parse(document);
            System.out.println("By DOMParser: " + candieList);
//        } else {
//            System.out.println("XML document failed validation.");
//        }
    }
}
