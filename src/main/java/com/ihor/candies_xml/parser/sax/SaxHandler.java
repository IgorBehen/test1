package com.ihor.candies_xml.parser.sax;

import com.ihor.candies_xml.model.Candy;
import com.ihor.candies_xml.model.Ingredients;
import com.ihor.candies_xml.model.Type;
import com.ihor.candies_xml.model.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class SaxHandler extends DefaultHandler {
    static final String CANDIE_ID_TAG = "candieId";
    static final String CANDIE_TAG = "candie";
    static final String NAME_TAG = "name";
    static final String PRODUCTION_TAG = "production ";
    static final String ENERGY_TAG = "energy";
    static final String VALUE_TAG = "value";
    static final String TYPE_TAG = "type";
    static final String IRIS_TAG = "iris";
    static final String CARAMEL_TAG = "caramel";
    static final String CHOCOLATE_TAG = "chocolate";
    static final String ISSTUFFED_TAG = "isStuffed";
    static final String FAT_TAG = "fat";
    static final String CARBOHYDRATE_TAG = "carbohydrate";
    static final String PROTEIN_TAG = "protein";
    static final String INGREDIENTS_TAG = "ingredients";

    private List<Candy> candies = new ArrayList<>();
    private Candy candy;
    private Type type;
    private Value value;
    private List<Ingredients> ingredients;
    private String currentElement;

    public List<Candy> getCandieList() {
        return this.candies;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        currentElement = qName;
        switch (currentElement) {
            case CANDIE_TAG: {
                String candieId = attributes.getValue(CANDIE_ID_TAG);
                candy = new Candy();
                candy.setCandieId(Integer.parseInt(candieId));
                break;
            }
            case TYPE_TAG: {
                type = new Type();
                break;
            }
            case VALUE_TAG: {
                value = new Value();
                break;
            }
            case INGREDIENTS_TAG: {
                ingredients = new ArrayList<>();
                break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case CANDIE_TAG: {
                candies.add(candy);
                break;
            }
            case TYPE_TAG: {
                candy.setType(type);
                type = null;
                break;
            }
            case VALUE_TAG: {
                candy.setValue(value);
                value = null;
                break;
            }
            case INGREDIENTS_TAG: {
                candy.setIngredients((Ingredients) ingredients);
                ingredients = null;
                break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        if (currentElement.equals(NAME_TAG)) {
            candy.setName(new String(ch, start, length));
        }
        if (currentElement.equals(PRODUCTION_TAG)) {
            candy.setProduction(new String(ch, start, length));
        }
        if (currentElement.equals(ENERGY_TAG)) {
            candy.setEnergy(Integer.parseInt(new String(ch, start, length)));
        }
        if (currentElement.equals(CARAMEL_TAG)) {
            type.setCaramel(Boolean.parseBoolean(new String(ch, start, length)));
        }
        if (currentElement.equals(IRIS_TAG)) {
            type.setIris(Boolean.parseBoolean(new String(ch, start, length)));
        }
        if (currentElement.equals(CHOCOLATE_TAG)) {
            type.setChocolate(Boolean.parseBoolean(new String(ch, start, length)));
        }
        if (currentElement.equals(ISSTUFFED_TAG)) {
            type.setIsStuffed(Boolean.parseBoolean(new String(ch, start, length)));
        }
        if (currentElement.equals(PROTEIN_TAG)) {
            value.setProtein(Integer.parseInt(new String(ch, start, length)));
        }
        if (currentElement.equals(FAT_TAG)) {
            value.setFat(Integer.parseInt(new String(ch, start, length)));
        }
        if (currentElement.equals(CARBOHYDRATE_TAG)) {
            value.setCarbohydrate(Integer.parseInt(new String(ch, start, length)));
        }
    }
}
