package com.ihor.candies_xml.parser.sax;

import com.ihor.candies_xml.model.Candy;

import java.io.File;
import java.util.List;

public class Main {

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\candies.xml");
    File xsdFile = new File("src\\main\\resources\\candiesXSD.xsd");

    List<Candy> candies = SaxParser.parse(xmlFile, xsdFile);
    System.out.println(candies);
  }
}
