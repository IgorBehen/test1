package com.ihor.candies_xml.parser.sax;

import com.ihor.candies_xml.model.Candy;

import com.ihor.candies_xml.xmlValidator.XmlValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SaxParser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Candy> parse(File xml, File xsd) {
    List<Candy> candies = new ArrayList<>();
    try {
      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
      saxParserFactory.setValidating(true);

      SAXParser saxParser = saxParserFactory.newSAXParser();
      System.out.println(saxParser.isValidating());
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(xml, saxHandler);
      candies = saxHandler.getCandieList();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }
    return candies;
  }
}