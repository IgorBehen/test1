package com.ihor.candies_xml.comparator;

import com.ihor.candies_xml.model.Candy;
import java.util.Comparator;

class CandyComparator implements Comparator<Candy> {

  @Override
  public int compare(Candy o1, Candy o2) {
    return o1.getCandieId() - o2.getCandieId();
  }
}
