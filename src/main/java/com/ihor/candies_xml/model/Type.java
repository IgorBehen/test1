package com.ihor.candies_xml.model;

public class Type
{
    private boolean iris;

    private boolean caramel;

    //private CandyGroup candyGroup;
    private boolean chocolate;

    private boolean isStuffed;

    public boolean getIris ()
    {
        return iris;
    }

    public void setIris (boolean iris)
    {
        this.iris = iris;
    }

    public boolean getCaramel ()
    {
        return caramel;
    }

    public void setCaramel (boolean caramel)
    {
        this.caramel = caramel;
    }

//    public CandyGroup getCandyGroup ()
//    {
//        return candyGroup;
//    }
//
//    public void setCandyGroup (CandyGroup candyGroup)
//    {
//        this.candyGroup = candyGroup;
//    }

    public boolean getIsStuffed ()
    {
        return isStuffed;
    }

    public void setIsStuffed (boolean isStuffed)
    {
        this.isStuffed = isStuffed;
    }

    public boolean getChocolate ()
    {
        return chocolate;
    }

    public void setChocolate (boolean chocolate)
    {
        this.chocolate = chocolate;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [iris = "+iris+", caramel = "+caramel+/*", candyGroup = "+candyGroup+*/", isStuffed = "+isStuffed+", chocolate = "+chocolate+"]";
    }
}
