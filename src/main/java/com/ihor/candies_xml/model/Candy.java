package com.ihor.candies_xml.model;

public class Candy
{
    private int candieId;

    private String production;

    private String name;

    private Ingredients ingredients;

    private Type type;

    private Value value;

    private int energy;

    public int getCandieId ()
    {
        return candieId;
    }

    public void setCandieId (int candieId)
    {
        this.candieId = candieId;
    }

    public String getProduction ()
    {
        return production;
    }

    public void setProduction (String production)
    {
        this.production = production;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public Ingredients getIngredients ()
    {
        return ingredients;
    }

    public void setIngredients (Ingredients ingredients)
    {
        this.ingredients = ingredients;
    }

    public Type getType ()
    {
        return type;
    }

    public void setType (Type type)
    {
        this.type = type;
    }

    public Value getValue ()
    {
        return value;
    }

    public void setValue (Value value)
    {
        this.value = value;
    }

    public int getEnergy ()
    {
        return energy;
    }

    public void setEnergy (int energy)
    {
        this.energy = energy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [candieId = "+candieId+", production = "+production+", name = "+name+", ingredients = "+ingredients+", type = "+type+", value = "+value+", energy = "+energy+"]. \n";
    }
}
