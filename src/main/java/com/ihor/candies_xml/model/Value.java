package com.ihor.candies_xml.model;

public class Value
{
    private int carbohydrate; 

    private int fat;

    private int protein;

    //private CandyGroup candyGroup;

    public int getCarbohydrate ()
    {
        return carbohydrate;
    }

    public void setCarbohydrate (int carbohydrate)
    {
        this.carbohydrate = carbohydrate;
    }

    public int getFat ()
    {
        return fat;
    }

    public void setFat (int fat)
    {
        this.fat = fat;
    }

    public int getProtein ()
    {
        return protein;
    }

    public void setProtein (int protein)
    {
        this.protein = protein;
    }

   /* public CandyGroup getCandyGroup ()
    {
        return candyGroup;
    }

    public void setCandyGroup (CandyGroup candyGroup)
    {
        this.candyGroup = candyGroup;
    }*/

    @Override
    public String toString()
    {
        return "ClassPojo [carbohydrate = "+carbohydrate+", fat = "+fat+", protein = "+/*proteins+", candyGroup = "+candyGroup+*/"]";
    }
}
