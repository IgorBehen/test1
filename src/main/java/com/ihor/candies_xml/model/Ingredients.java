package com.ihor.candies_xml.model;

import java.util.List;

public class Ingredients
{
    private List<String> ingredient;

    public Ingredients(String ingredient) {
        this.ingredient.add(ingredient);
    }

    public List<String> getIngredient ()
    {
        return ingredient;
    }

    public void setIngredient (List<String> ingredient)
    {
        this.ingredient = ingredient;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [ingredient = "+ingredient+"]";
    }
}