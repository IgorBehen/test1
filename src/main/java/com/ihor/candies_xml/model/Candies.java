package com.ihor.candies_xml.model;

public class Candies
{
    private Candy[] candy;

    public Candy[] getCandy ()
    {
        return candy;
    }

    public void setCandy (Candy[] candy)
    {
        this.candy = candy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [candy = "+candy+"]";
    }
}
