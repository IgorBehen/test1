package com.ihor.e_streams.b_training;

import java.lang.management.OperatingSystemMXBean;
import java.util.*;
import java.util.stream.Stream;

public class B {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("John", 25));
        persons.add(new Person("Jack", 42));
        persons.add(new Person("Fil", 42));
        persons.add(new Person("Tom", 54));
        persons.add(new Person("Maike", 15));
        persons.add(new Person("Arny", 24));
        System.out.println(persons);

        Iterator<Person> personIterator = persons.iterator();
        while(personIterator.hasNext()){
            System.out.println("personIterator: " + personIterator.next());
        }

        Stream<Integer> integerStream = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 9);
        //integerStream.forEach(x -> System.out.print(x + ", "));
//       Integer min = integerStream
//               .min(Integer::compare).get();
//       System.out.println(min);

        Stream<Integer> integerStream1 = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 9);
        Optional<Integer> var = integerStream1
                .max(Comparator.reverseOrder()); // знаходимо min навпаки :)
        if(var.isPresent()){
            System.out.println(var.get());
        }
        else{
            System.out.println("NULL");
        }

        Stream<Integer> integerStream2 = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 9);
        long count = integerStream2.count();
        System.out.println(count);

        Stream<Integer> integerStream3 = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 8, 9);
         integerStream3.distinct().sorted().skip(3).forEach(x -> System.out.print(x + ", "));
        System.out.println("\n" + count);


        Stream.generate(new Random()::nextInt).limit(10).forEach(x -> System.out.print(x + ", "));

        System.out.println();

        Stream.generate(new Random()::nextDouble)
                .limit(10)
                .distinct()
                .sorted()
                .forEach(x -> System.out.print(x + ", "));

        boolean intNoneMatch = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 9).anyMatch(x -> x < 8);
        System.out.println("\n" + intNoneMatch);

        Optional<Integer> oi = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 9)/*.findAny();*/.findFirst();
        System.out.println(oi);
        if (oi.isPresent()) {
            System.out.println(oi.get());
        }
        else {
            System.out.println("no value");
        }

        Optional<Integer> oi1 = Optional.empty();
        System.out.println(oi1);
        if (oi1.isPresent()) {
            System.out.println(oi1.get());
        }
        else {
            System.out.println("no value");
        }


        boolean present = Stream.of(1, 3, 2, 4, 5, 7, 6, 8, 9).mapToInt(x -> x).average().isPresent();
        System.out.println(present);


        final List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        primes.stream()
                .mapToInt(i -> i)
                .average()
                .ifPresent(avg -> System.out.println("Average found is " + avg));

        final List<Integer> primes1 = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        // MIN
        primes1.stream()
                .min(Comparator.comparing(i -> i));
                //.ifPresent(min -> System.out.println("Min found is " + min));


        final List<Integer> primes2 = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        int i = primes2.stream().mapToInt(x -> x).sum();
        System.out.println("Sum is: " + i);

        List<Integer> primes3 = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
        IntSummaryStatistics stats = primes3.stream()
                .mapToInt((x) -> x)
                .summaryStatistics();
        System.out.println(stats);




    }
}
