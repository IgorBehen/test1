package com.ihor.e_streams.a;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
       persons.add(new Person("John", 35));
       persons.add(new Person("Jack", 42));
       persons.add(new Person("Fil", 42));
       persons.add(new Person("Tom", 54));
       persons.add(new Person("Maike", 15));
       persons.add(new Person("Arny", 24));
        System.out.println(persons);

        persons.stream()
                .filter(p -> p.getAge() >= 24 )
                //.sorted((a, b) -> a.getName().compareTo(b.getName()))
                .sorted(Comparator.comparing(Person::getName))
                //.peek(x -> System.out.print("#" + x + ", "))
                .map(Person::getName)
                //.peek(x -> System.out.print("!" + x + ", "))
                //.distinct()
                .forEach(person -> System.out.print("@" + person + " "));

        double average = persons.stream()
                .filter(x -> x.getAge() > 18)
                .mapToInt(y ->  y.getAge())
                .average()
                .getAsDouble();

        System.out.println("\naverage - " + average);

        persons.stream()
                .filter(p -> p.getAge() > 18)
                .sorted((p1, p2) -> p1.getName().compareTo(p2.getName()))
                .map(p -> p.getName())
                .forEach(
                        (String n) ->System.out.println(n)
                );


        persons.stream().forEach(p -> System.out.println(p));
    }

}
