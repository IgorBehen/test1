package com.ihor.e_streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class A {
    public static void main(String[] args) {
        List<Integer> collect = Arrays.asList(10, 20, -30, 40, 50, 50)
                .stream()
                .filter(x -> x > 0)
                .distinct()
                .sorted()
                //???.count();
                //???.min(Comparator.comparing((x, y) -> x.compareTo(y)));
                .collect(Collectors.toList());

        collect.forEach(System.out::println);

        //##################################

        Stream<Integer> integerStream = Stream.of(10, 20, 30);
        integerStream.forEach(x -> System.out.print(x + ", "));

        System.out.println();

        Integer[] arr = new Integer[5];
        A.fillToArray(arr);
        Arrays.stream(arr).forEach(d -> System.out.print(d + ", "));

        System.out.println();

        //---
        Stream<Integer> stream = Arrays.stream(arr);
        stream
                .mapToInt(z -> z  + 1)
                .forEach(System.out::println);


        Stream.builder()
                .add(11)
                .add(22)
                .add(33)
                .build()
                .limit(2)
                .forEach(a -> System.out.print(a + ", "));
        System.out.println();
        // або Ctrl + Alt + V - обгортаєм в ліст ---> :
        Stream<Object> build = Stream.builder().add(111).add(222).add(333).build();
        build.skip(1).forEach(a -> System.out.print(a + ", "));
        System.out.println();

        //#####################
        "Hello".chars();  // виділяєм і Ctrl + Alt + V ---> :
        IntStream chars = "Hello".chars();
        // вивід цифровими значеннями літер:
        //chars.forEach(c -> System.out.print(c + ", "));
        //або буквами:
        chars.skip(1).forEach(c -> System.out.print((char)c + ", "));


    }

    static /*Integer[] */ void fillToArray(Integer[] arr){
        int i = 1;
        for (int j = 0; j < arr.length; j++) {
            arr[j] = i;
            i++;
        }
        //return arr;
    }


}
