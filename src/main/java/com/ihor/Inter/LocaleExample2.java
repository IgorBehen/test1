package com.ihor.Inter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
/**
 * @author Igor
 * f,vorimirmimifmrf
 */
public class LocaleExample2 {
    /**
     *
     * @author Igor
     * @param args
     */
    public static void main(String[] args) {
        String s = (char) 27 + "[31m" ;
        String s0 = (char) 27 + "[43m";
        String s_s = (char) 27 + "[0m";
        String ss = s + s0;
        String s1 = (char) 27 + "[35m";
        String s2 = (char) 27 + "[34m";
        Locale enLocale = new Locale("en", "US");
        Locale frLocale = new Locale("fr", "FR");
        Locale esLocale = new Locale("es", "ES");
        System.out.println(ss + "English language name (default): "+ s_s + s1 +
                enLocale.getDisplayLanguage());
        System.out.println(s + "English language name in French: " + s1 +
                enLocale.getDisplayLanguage(frLocale));
        System.out.println(s + "English language name in spanish: " + s2 +
                enLocale.getDisplayLanguage(esLocale));
    }
}
