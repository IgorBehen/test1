package com.ihor.io_nio.io;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.*;

public class B {

    private static final Logger log = LogManager.getLogger(B.class);

    //  Print directory
    private void showContentDirectory() {
        System.out.println("Printing directory...");
        File file = new File("E:\\Igor\\EPAM_JAVA\\.Study Modules");
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            System.out.println("No directory");
        }
        System.out.println("This is end of directory files.");
    }
    private void printDirectory(File file, String str){
        System.out.println(str + "Directory: " + file.getName());
        str = str + " ";
        File[] fileNames = file.listFiles();
        for (File f : fileNames) {
            if (f.isDirectory()) {
                printDirectory(f, str);
            } else {
                System.out.println(str + "File: " + f.getName());
            }
        }

    }
    public static void main(String[] args) throws IOException {
        B b = new B();
        b.showContentDirectory();




//without buffering

//        int count = 0;
//        System.out.println("Reading....");
//        InputStream inputStream = new FileInputStream("why140mb.pdf");
//        int data = inputStream.read();
//
//        long start = System.currentTimeMillis();
//
//        while(data != -1){
//            data = inputStream.read();
//            count++;
//        }
//        inputStream.close();
//        System.out.println("count: " + count);
//        System.out.println("Done");
//        long end = System.currentTimeMillis();
//        float sec = (end - start) / 1000F;
//        System.out.println(sec + " seconds");
//        float min = (end - start) /(60*1000F);
//        System.out.println(min + " minutes");

// with buffering

//        int count1 = 0;
//        System.out.println("Reading buffering....");
//        DataInputStream disB = new DataInputStream(
//                new BufferedInputStream(
//                        new FileInputStream("why140mb.pdf")));
//        long start = System.currentTimeMillis();
//
//        try{
//            while(true){
//                byte b0 = disB.readByte();
//                count1++;
//            }
//        }catch (EOFException e){
//        }
//        disB.close();
//        System.out.println("count: " + count1);
//        System.out.println("Done");
//        long end = System.currentTimeMillis();
//        float sec = (end - start) / 1000F;
//        System.out.println(sec + " seconds");


//with customizable buffer

//        int bufferSize = 1 * 1024 * 1024;
//        int count2 = 0;
//        System.out.println("Reading buffering....");
//        DataInputStream disB1 = new DataInputStream(
//                new BufferedInputStream(
//                        new FileInputStream("111.pdf"), bufferSize));
//        try{
//            while(true){
//                byte b2  = disB1.readByte();
//                count2++;
//            }
//        }catch (EOFException e){
//        }
//        disB1.close();
//        System.out.println("count: " + count2);
//        System.out.println("Done");













     }
}



















