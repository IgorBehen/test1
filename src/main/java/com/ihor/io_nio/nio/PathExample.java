package com.ihor.io_nio.nio;
import java.io.IOException;
import java.nio.file.*;


public class PathExample {
    public static void main(String[] args) throws IOException {
/**
 * http://www.quizful.net/post/java-nio-tutorial
 */
        // Cоздание объекта Path через вызов статического метода get() класса Paths
        Path testFilePath = Paths.get("D:\\test\\testfile.txt");

        //Вывод инормации о файле
        System.out.println("Printing file information: ");
        System.out.println("\t file name: " + testFilePath.getFileName());
        System.out.println("\t root of the path: " + testFilePath.getRoot());
        System.out.println("\t parent of the target: "
                + testFilePath.getParent());

        //Вывод элементов пути
        System.out.println("Printing elements of the path: ");
        for (Path element : testFilePath) {
            System.out.println("\t path element: " + element);
        }

  //**************   пример который включает получение абсолютного пути от относительного пути и нормализацию пути:
        Path testFilePath1 = Paths.get(".\\Test");

        System.out.println("The file name is: " + testFilePath.getFileName());
        System.out.println("It's URI is: " + testFilePath1.toUri());
        System.out.println("It's absolute path is: "
                + testFilePath1.toAbsolutePath());
        System.out.println("It's normalized path is: "
                + testFilePath1.normalize());

        //Получение другого объекта строки по нормализованному относительному пути
        Path testPathNormalized = Paths
                .get(testFilePath1.normalize().toString());
        System.out.println("It's normalized absolute path is: "
                + testPathNormalized.toAbsolutePath());
        System.out.println("It's normalized real path is: "
                + testFilePath1.toRealPath(LinkOption.NOFOLLOW_LINKS));
    }
}

