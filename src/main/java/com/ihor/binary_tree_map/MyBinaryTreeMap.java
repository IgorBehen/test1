package com.ihor.binary_tree_map;

import java.util.*;

//��� ������ ������ �������� binary tree map

public class MyBinaryTreeMap <T extends Comparable<T>> implements Map{

     Node root;

    class Node {
        T value;
        T data;
        Node left;
        Node right;

        public Node(T value ) {
            this.value = value ;
        }
    }

    private void insert(Node node, T value) {

        if(value.compareTo(node.value) < 0) {
            if(node.left == null)
                node.left = new Node(value);
            else
                insert(node.left, value);
        }
        else {
            if(node.right == null)
                node.right = new Node(value);
            else
                insert(node.right, value);
        }
    }


    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean containsKey(Object key) {
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        return false;
    }

    @Override
    public Object get(Object key) {
        return null;
    }

    @Override
    public Object put(Object key, Object value) {
        return null;
    }

    @Override
    public Object remove(Object key) {
        return null;
    }

    @Override
    public void putAll(Map m) {

    }

    @Override
    public void clear() {
       root = null;
    }

    @Override
    public Set keySet() {
        return null;
    }

    @Override
    public Collection values() {
        return null;
    }

    @Override
    public Set<Entry> entrySet() {
        return null;
    }


}
