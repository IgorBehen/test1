package com.ihor.binary_tree_map.one;

import java.util.Map;

class Entry<K, V> implements Map.Entry<K, V> {
    private final K key;
    private V value;

    public Entry(final K key, final V value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return key + "=>" + value;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(final V value) {
        final V old = this.value;
        this.value = value;
        return old;
    }
}
