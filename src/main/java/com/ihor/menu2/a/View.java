package com.ihor.menu2.a;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.ihor.menu2.a.Constant.*;

public class View {

    private Controller controller;
    Map<Integer, String> items;

    public View() {
        controller = new Controller();
        items = new LinkedHashMap<>();
        items.put(1, bundle.getString("item1"));
        items.put(2, bundle.getString("item2"));
        items.put(3, bundle.getString("item3"));
        items.put(4, bundle.getString("item4"));
        items.put(5, bundle.getString("item5"));
        items.put(0, bundle.getString("exit"));
    }

    public void menuListItems() {
        System.out.println("\nMENU:");
        for (String item : items.values()) {
            System.out.println(item);
        }
        System.out.println();
        log.info(bundle.getString("user_selection"));
        int selection = scan.nextInt();
        switch (selection) {
            case 0:
                System.out.println(bundle.getString("bye"));
                System.exit(0);
            case 1:
                controller.getTask1();
                menuListItems();
            case 2:
                controller.getTask2();
                menuListItems();
            case 3:
                controller.getTask3();
                menuListItems();
            case 4:
                controller.getTask4();
                menuListItems();
                break;
            case 5:
                controller.getTask5();
                menuListItems();
            default:
                log.info(bundle.getString("invalid_input"));
                menuListItems();
        }
    }
}

