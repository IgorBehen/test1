package com.ihor.menu2.a;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;
import java.util.Scanner;

public class Constant {
    public static Scanner scan = new Scanner(System.in);
    public static Logger log = LogManager.getLogger(Constant.class);
    public static ResourceBundle bundle = ResourceBundle.getBundle("strings");
}