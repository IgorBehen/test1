package com.ihor.menu2.example;//package com.ihor.menu2;
//
//import java.io.IOException;
//import java.util.LinkedHashMap;
//import java.util.Map;
//
//import static com.ihor.menu2.Constant.*;
//
//public class View {
//
//    private Controller controller;
//    Map<String, String> map;
//
//    public View() {
//        controller = new Controller();
//        map = new LinkedHashMap<>();
//        map.put("1", bundle.getString("task1"));
//        map.put("2", bundle.getString("task2"));
//        map.put("3", bundle.getString("task3"));
//        map.put("4", bundle.getString("task4"));
//        map.put("5", bundle.getString("task5"));
//        map.put("Q", bundle.getString("Q"));
//    }
//
//    public void outputMenu() throws IOException {
//        System.out.println("\nMENU:");
//        for (String str : map.values()) {
//            logger.info(str);
//        }
//        logger.info(bundle.getString("choice"));
//        String userChoice = scanner.nextLine().toUpperCase();
//        switch (userChoice) {
//            case "1":
//                controller.getTask1();
//                outputMenu();
//            case "2":
//                controller.getTask2();
//                outputMenu();
//            case "3":
//                controller.getTask3();
//                outputMenu();
//            case "4":
//                controller.getTask4();
//                outputMenu();
//                break;
//            case "5":
//                controller.getTask5();
//                outputMenu();
//            case "Q":
//                System.exit(0);
//            default:
//                logger.info(bundle.getString("invalid_input"));
//                outputMenu();
//        }
//    }
//}
//
