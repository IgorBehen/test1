package com.ihor.f_optional;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class A {
    public static void main(String[] args) {
        Optional<Integer> op = Optional.empty();
        Optional<Integer> op1 = Optional.of(19);
        Optional<Integer> op2 = Optional.ofNullable(null);

        System.out.println(op);
        System.out.println(op1);
        System.out.println(op2);
    }

}

