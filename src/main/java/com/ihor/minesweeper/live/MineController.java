package com.ihor.minesweeper.live;

public interface MineController {

    boolean updateArea(int row, int column);

}

