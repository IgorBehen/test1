package com.ihor.g_my_exception;

import javax.xml.transform.Source;

public class A {
    public static void main(String[] args) throws MyException {
        A a = new A();
        //a.checkMyException();
        a.checkMyRuntimeException();

}
     void checkMyException() throws MyException {
        throw new MyException("This is my MyException!");
    }
     void checkMyRuntimeException()  {
        throw new MyRuntimeException("This is my MyRuntimeException!");
    }
}


class MyException extends Exception{
    MyException(String myException) {
        super(myException);
    }
}

class MyRuntimeException extends RuntimeException{
    MyRuntimeException(String myRuntimeException) {
        super(myRuntimeException);
    }
}




class B extends A{
    public static void main(String[] args) throws MyException {
        A a = new B();
        a.checkMyException();

        MyException myException = new MyException("Printing: printStackTrace()");
                myException.printStackTrace();
    }
    @Override
    void  checkMyException() throws MyException {
        int x = 1;
        int y = 1;
        x = x + y;
        System.out.println("!@# " + x);

        //throw new MyException("This is my MyException!");
        //throw new NullPointerException("This is NullPointerException");
    }
}