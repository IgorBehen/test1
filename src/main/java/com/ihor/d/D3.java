package com.ihor.d;

import java.util.Objects;

public class D3 {
    Integer a;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        D3 d3 = (D3) o;
        return Objects.equals(a, d3.a);
    }

    @Override
    public int hashCode() {
        return Objects.hash(a);
    }

    public static void main(String[] args) {

    }



    @FunctionalInterface  //RetentionPolicy: SOURCE, CLASS, RUNTIME
    interface AAA{
        void aaa();
    }
}
