package com.ihor.d;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class D1 {

    public static void main(String[] args) {
        Deque<String> deque = new ArrayDeque<String>();
        System.out.println("'1'  -  " + deque.add(new String("5")));
        System.out.println("'1'  -  " + deque.add(new String("5")));
        deque.addFirst("A");
        deque.add(new String("7"));
        deque.addFirst("B");
        printDeque(deque);
        System.out.println();
        //deque.addLast(new Integer(5));//ошибка компиляции
        System.out.println("'peek'  - " + deque.peek());
        System.out.println("Before:");
        printDeque(deque);
        System.out.println();
        System.out.println("'poll' - " + deque.pollFirst());
        System.out.println("remove: " + deque.remove("A"));
        System.out.println("After:");
        printDeque(deque);
        System.out.println();
        System.out.println(deque.contains("7"));
        System.out.println(deque.size());
        Object[] arrDeque = deque.toArray();
        System.out.println(Arrays.toString(arrDeque));
    }

    public static void printDeque(Deque<?> d){
        for (Object de : d)
            System.out.print(de + "; ");
    }
}
