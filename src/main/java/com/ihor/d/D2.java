package com.ihor.d;

import java.util.*;

public class D2 {
    public static void main(String[] args) {
        ArrayList <String> al = new ArrayList<>();
        al.add("abc");
        al.add(1, "def");
        al.add(2, "ghi");
        al.add(3, "jkl");
        System.out.println(al);
        System.out.println(al.size());
        String s1 = al.remove(al.size()-1);
        al.add(0, s1);
        System.out.println(al);

        for (int i = al.size()-1; i >= 0; i--) {
            System.out.print(al.get(i) + ", ");
        }
        System.out.println();
        int[] ar = new int[10];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = i;
            System.out.print(i + ", ");
        }
        System.out.println();
        int a = ar.length;
        for (int i = 0; i < ar.length; i++) {
            a--;
            System.out.print(ar[a] + ", ");
        }

        System.out.println();
//        Date currentTime = new Date();
//        System.out.println(currentTime);
//        for (int i = 1; i <= 100000; i++) {
//            System.out.print(i + " ");
//        }
//        System.out.println();
//        Date newTime = new Date();
//        System.out.println(newTime);
//        long dif = newTime.getTime() - currentTime.getTime();
//        System.out.println(dif);

        Queue<String> qs = new LinkedList<>();
        qs.add("a");
        qs.add("b");
        qs.add("c");
        System.out.println(qs);

        List<String> ls = new LinkedList<>();
        ls.add("1");
        ls.add("2");
        ls.add("3");
        System.out.println(ls);

        Deque<String> dq = new ArrayDeque<>();
        System.out.println(isAdd(dq));
        System.out.println(isAdd1(dq));
        System.out.println(dq.add("333"));
        System.out.println(dq);


    }

    private static boolean isAdd1(Deque<String> dq) {
        return dq.add("222");
    }

    private static boolean isAdd(Deque<String> dq) {
        return dq.add("111");
    }
}
