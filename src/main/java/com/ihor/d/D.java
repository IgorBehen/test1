package com.ihor.d;

import org.apache.logging.log4j.core.util.JsonUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class D {
    public static void main(String[] args) {
        Integer i = 5;
        String s =  i.toString();
        System.out.println(s);
        String str = "Hello";
        System.out.println(str.replace('l', 'N'));
        System.out.println(str.toLowerCase());
        System.out.println(str.toUpperCase());
        Pattern p =  Pattern.compile("\\.+");
        Matcher m =  p.matcher("asd");
        System.out.println(m.matches());
        Collection<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Collection<Integer> list1 = new ArrayList<>();
        list1.addAll(list);
        System.out.println("List: " + list + " - " + list.size() + " \n" + "List1: " + list1 + " - " + list1.size());
        list.clear();
        System.out.println("List: " + list + " - " + list.size() + " \n" + "List1: " + list1 + " - " + list1.size());
        for (Integer integer : list1) {
            System.out.print(integer + " ");
        }
        System.out.println();
        int index = 0;
        for (Integer integer : list1) {
            integer = integer * 32;
            index++;
            System.out.print(index + ")" + integer + " ");
            if(index == 2){
                System.out.print("/(" + integer + ")  ");
            }
        }
        System.out.println();
        Object[] array = list1.toArray();
        System.out.println("array: " + Arrays.toString(array));

        int[] arr = new int[5];
        arr[0] = 11;
        arr[1] = 22;
        arr[2] = 33;
        System.out.println("arr: " + arr.toString());
        System.out.println("arr: " + Arrays.toString(arr));
        for (int j = 0 ; j < arr.length; j++) {
            arr[j] = arr[j] + 1;
            System.out.print(arr[j] + " ");
        }
        System.out.println();
    List<String> lsStr = new LinkedList<>();
        lsStr.add("abc");
        lsStr.add("123");
        lsStr.add("xyz");
        lsStr.add("xyz");
        lsStr.add("987");
        lsStr.add("");
        lsStr.add("-+=");
        System.out.println(lsStr);
        /*List<String> lsStrStr =*/
        System.out.println(lsStr.stream().peek(System.out::println).filter(x -> !x.isEmpty()).map(x -> x + " ! ")
                .distinct().collect(Collectors.toList()));
        //System.out.println(lsStrStr.toString());



    }


}
