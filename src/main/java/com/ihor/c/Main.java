package com.ihor.c;

import java.io.*;

public class Main {
    public static void main(String[] args)  {
        File f = new File("E:\\Igor\\EPAM_JAVA\\my_repos\\TestIdea1\\src\\main\\java\\com\\ihor\\c\\test_file.txt");
        Person person = new Person("First9", 2, 5, 987.00056);
        Person.setDb(555.555);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));){
            oos.writeObject(person);

            Person p = (Person) ois.readObject();
            System.out.println(p);
        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }
}
