package com.ihor.c;

import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable {
    String str;
    int i;
    transient Integer in;
    static Double db = 123.56;

    public Person(String str, int i, Integer in, Double db) {
        this.str = str;
        this.i = i;
        this.in = in;
        this.db = db;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public int getI() {
        return i;
    }

    public void setI(int i) {
        this.i = i;
    }

    public Integer getIn() {
        return in;
    }

    public void setIn(Integer in) {
        this.in = in;
    }

    public static Double getDb() {
        return db;
    }

    public static void setDb(Double db) {
        Person.db = db;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return i == person.i &&
                Objects.equals(str, person.str) &&
                Objects.equals(in, person.in);
    }

    @Override
    public int hashCode() {
        return Objects.hash(str, i, in);
    }

    @Override
    public String toString() {
        return "Person{" +
                "str='" + str + '\'' +
                ", i=" + i +
                ", in=" + in +
                ", db=" + db +
                '}';
    }
}
