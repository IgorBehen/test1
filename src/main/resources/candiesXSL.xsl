<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEA">
        <div style="background-color: yellow; color: black;">
          <h2>C A N D I E S</h2>
        </div>
        <table border="3">
          <tr bgcolor="#fe2eb9">
            <th>Candie ID</th>
            <th>Name</th>
            <th>Production</th>
            <th>Energy</th>
            <th>Iris</th>
            <th>Caramel</th>
            <th>Chocolate</th>
            <th>isStuffed</th>
            <th>Fat</th>
            <th>Carbohydrate</th>
            <th>Protein</th>
            <th>Ingredients</th>
          </tr>
          <xsl:for-each select="candies/candie">
            <tr>
              <td><xsl:value-of select="@candieId"/></td>
              <td><xsl:value-of select="name"/></td>
              <td><xsl:value-of select="production"/></td>
              <td>
                <xsl:value-of select="energy "/>
                <xsl:text> kcal</xsl:text>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/iris ='true'">
                    <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/caramel ='true'">
                    <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/chocolate ='true'">
                    <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:choose>
                  <xsl:when test="types/isStuffed ='true'">
                  <xsl:text>YES</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:text>NO</xsl:text>
                  </xsl:otherwise>
                </xsl:choose>
              </td>
              <td>
                <xsl:value-of select="value/fat"/>
                <xsl:text> gram</xsl:text>
              </td>
              <td>
                <xsl:value-of select="value/carbohydrate"/>
                <xsl:text> gram</xsl:text>
              </td>
              <td>
                <xsl:value-of select="value/protein"/>
                <xsl:text> gram</xsl:text>
              </td>
            </tr>
            <td>
              <xsl:for-each select="Ingredients">
                <xsl:value-of select="."/>
                <xsl:text>, </xsl:text>
              </xsl:for-each>
            </td>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>